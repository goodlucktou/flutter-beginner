import 'package:state_management_redux/counter/counter_actions.dart';
import 'package:state_management_redux/counter/counter_state.dart';

CounterState counterReducer(CounterState state, dynamic action) {
  if (action is CounterAddAction) {
    return CounterState(state.count + 1);
  }

  if (action is CounterSubAction) {
    return CounterState(state.count - 1);
  }

  return state;
}

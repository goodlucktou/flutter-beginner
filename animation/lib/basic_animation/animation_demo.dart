import 'package:flutter/material.dart';

class AnimtionDemo extends StatefulWidget {
  const AnimtionDemo({Key? key}) : super(key: key);

  @override
  _AnimtionDemoState createState() => _AnimtionDemoState();
}

class _AnimtionDemoState extends State<AnimtionDemo>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animation = Tween<double>(begin: 40, end: 100).animate(controller)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animation 动画'),
      ),
      body: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Icon(
              Icons.favorite,
              color: Colors.red[100],
              size: animation.value * 1.5,
            ),
            Icon(
              Icons.favorite,
              color: Colors.red[400],
              size: animation.value,
            ),
            Icon(
              Icons.favorite,
              color: Colors.red[600],
              size: animation.value / 2,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow, color: Colors.white),
        onPressed: () {
          if (controller.status == AnimationStatus.completed) {
            controller.reverse();
          } else {
            controller.forward();
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

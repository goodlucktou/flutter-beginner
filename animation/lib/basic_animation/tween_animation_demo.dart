import 'package:flutter/material.dart';

class TweenAnimationDemo extends StatefulWidget {
  TweenAnimationDemo({Key? key}) : super(key: key);

  @override
  _TweenAnimationDemoState createState() => _TweenAnimationDemoState();
}

class _TweenAnimationDemoState extends State<TweenAnimationDemo> {
  var _sliderValue = 0.0;
  Color _newColor = Colors.orange;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TweenAnimationBuilder'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          children: [
            TweenAnimationBuilder(
              tween: ColorTween(
                begin: Colors.white,
                end: _newColor,
              ),
              duration: Duration(seconds: 1),
              builder: (_, color, child) {
                return ColorFiltered(
                  colorFilter:
                      ColorFilter.mode(color as Color, BlendMode.modulate),
                  child: ClipOval(
                    child: ClipOval(
                      child: Image.asset(
                        'images/beauty.jpeg',
                        width: 300,
                      ),
                    ),
                  ),
                );
              },
            ),
            Slider.adaptive(
              value: _sliderValue,
              onChanged: (value) {
                setState(() {
                  _sliderValue = value;
                });
              },
              onChangeEnd: (value) {
                setState(() {
                  _newColor = _newColor.withRed((value * 255).toInt());
                });
              },
            )
          ],
        ),
      ),
    );
  }
}

class RepeatBall extends StatefulWidget {
  RepeatBall({Key? key}) : super(key: key);

  @override
  _RepeatBallState createState() => _RepeatBallState();
}

class _RepeatBallState extends State<RepeatBall> {
  final roundSize = 300.0;
  final ballSize = 40.0;

  var _tween = Tween<double>(
    begin: 0,
    end: 260,
  );
  @override
  Widget build(BuildContext context) {
    final margin = roundSize - ballSize;

    return Scaffold(
      appBar: AppBar(
        title: Text('TweenAnimationBuilder'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(children: [
          ClipOval(
            child: Container(
              width: roundSize,
              height: roundSize,
              color: Colors.white,
            ),
          ),
          TweenAnimationBuilder<double>(
            curve: Curves.easeInOut,
            duration: Duration(seconds: 2),
            tween: _tween,
            onEnd: () {
              setState(() {
                if (_tween.end == margin) {
                  _tween = Tween<double>(
                    begin: margin,
                    end: 0,
                  );
                } else {
                  _tween = Tween<double>(
                    begin: 0,
                    end: margin,
                  );
                }
              });
            },
            builder: (_, offset, child) {
              return Positioned(
                top: margin / 2,
                height: ballSize,
                left: offset,
                child: ClipOval(
                  child: Container(
                    width: ballSize,
                    height: ballSize,
                    color: Colors.blue,
                  ),
                ),
              );
            },
          ),
        ]),
      ),
    );
  }
}

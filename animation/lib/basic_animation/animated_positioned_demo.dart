import 'package:flutter/material.dart';

class AnimatedPositionedDemo extends StatefulWidget {
  AnimatedPositionedDemo({Key? key}) : super(key: key);

  @override
  _AnimatedPositionedDemoState createState() => _AnimatedPositionedDemoState();
}

class _AnimatedPositionedDemoState extends State<AnimatedPositionedDemo> {
  final roundSize = 200.0;
  var ballSize = 50.0;
  late List<Offset> animatedOffsets;
  int index = 0;
  @override
  void initState() {
    animatedOffsets = [
      Offset(0.0, (roundSize - ballSize) / 2),
      Offset((roundSize - ballSize) / 2, roundSize - ballSize),
      Offset(roundSize - ballSize, (roundSize - ballSize) / 2),
      Offset((roundSize - ballSize) / 2, 0.0),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedPositioned'),
        brightness: Brightness.dark,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Stack(children: [
          ClipOval(
            child: Container(
              width: roundSize,
              height: roundSize,
              color: Colors.white,
            ),
          ),
          AnimatedPositioned(
            top: animatedOffsets[index].dy,
            height: ballSize,
            left: animatedOffsets[index].dx,
            width: ballSize,
            child: ClipOval(
              child: Container(
                color: Colors.blue,
              ),
            ),
            duration: Duration(seconds: 2),
            curve: Curves.bounceIn,
            onEnd: () {
              setState(() {
                index = (index + 2) % animatedOffsets.length;
              });
            },
          ),
          AnimatedPositioned(
            top: animatedOffsets[(index + 1) % animatedOffsets.length].dy,
            height: ballSize,
            left: animatedOffsets[(index + 1) % animatedOffsets.length].dx,
            width: ballSize,
            child: ClipOval(
              child: Container(
                color: Colors.orange,
              ),
            ),
            duration: Duration(seconds: 2),
            curve: Curves.bounceIn,
          ),
          AnimatedPositioned(
            top: animatedOffsets[(index + 2) % animatedOffsets.length].dy,
            height: ballSize,
            left: animatedOffsets[(index + 2) % animatedOffsets.length].dx,
            width: ballSize,
            child: ClipOval(
              child: Container(
                color: Colors.green,
              ),
            ),
            duration: Duration(seconds: 2),
            curve: Curves.bounceIn,
          ),
          AnimatedPositioned(
            top: animatedOffsets[(index + 3) % animatedOffsets.length].dy,
            height: ballSize,
            left: animatedOffsets[(index + 3) % animatedOffsets.length].dx,
            width: ballSize,
            child: ClipOval(
              child: Container(
                color: Colors.red,
              ),
            ),
            duration: Duration(seconds: 2),
            curve: Curves.bounceIn,
          ),
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text(
          '走你',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        onPressed: () {
          setState(() {
            index = (index + 2) % 4;
          });
        },
      ),
    );
  }
}

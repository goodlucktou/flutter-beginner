import 'package:flutter/material.dart';

class ScaleTransitionDemo extends StatefulWidget {
  ScaleTransitionDemo({Key? key}) : super(key: key);

  @override
  _ScaleTransitionDemoState createState() => _ScaleTransitionDemoState();
}

class _ScaleTransitionDemoState extends State<ScaleTransitionDemo>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller =
      AnimationController(duration: const Duration(seconds: 10), vsync: this)
        ..repeat();

  //使用自定义曲线动画过渡效果
  late Animation<double> _animation =
      CurvedAnimation(parent: _controller, curve: Curves.easeOut);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScaleTransition'),
        brightness: Brightness.dark,
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: balloon(),
      ),
    );
  }

  @override
  void dispose() {
    _controller.stop();
    _controller.dispose();
    super.dispose();
  }

  Widget balloon() {
    return ScaleTransition(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        'images/balloon.png',
      ),
      scale: _animation,
    );
  }
}

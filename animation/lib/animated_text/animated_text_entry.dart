import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class AnimatedTextEntryPage extends StatelessWidget {
  const AnimatedTextEntryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('文字动画库')),
      body: Center(
        child: typerText(['我是岛上码农', '微信公众号同名', '微信号：island-coder']),
      ),
    );
  }

  Widget liquidText(String text) {
    return SizedBox(
      width: 320.0,
      child: TextLiquidFill(
        text: text,
        waveColor: Colors.blue[400]!,
        boxBackgroundColor: Colors.redAccent,
        textStyle: TextStyle(
          fontSize: 80.0,
          fontWeight: FontWeight.bold,
        ),
        boxHeight: 300.0,
        loadUntil: 0.7,
      ),
    );
  }

  Widget wavyText(List<String> texts) {
    return DefaultTextStyle(
      style: const TextStyle(
        color: Colors.blue,
        fontSize: 20.0,
      ),
      child: AnimatedTextKit(
        animatedTexts: texts.map((e) => WavyAnimatedText(e)).toList(),
        isRepeatingAnimation: true,
        repeatForever: true,
        onTap: () {
          print("文字点击事件");
        },
      ),
    );
  }

  Widget rainbowText(List<String> texts) {
    const colorizeColors = [
      Colors.purple,
      Colors.blue,
      Colors.yellow,
      Colors.red,
    ];

    const colorizeTextStyle = TextStyle(
      fontSize: 36.0,
      fontWeight: FontWeight.bold,
    );
    return SizedBox(
      width: 320.0,
      child: AnimatedTextKit(
        animatedTexts: texts
            .map((e) => ColorizeAnimatedText(
                  e,
                  textAlign: TextAlign.center,
                  textStyle: colorizeTextStyle,
                  colors: colorizeColors,
                ))
            .toList(),
        isRepeatingAnimation: true,
        repeatForever: true,
        onTap: () {
          print("文字点击事件");
        },
      ),
    );
  }

  Widget rotateText(List<String> texts) {
    return SizedBox(
      width: 320.0,
      height: 100.0,
      child: DefaultTextStyle(
        style: const TextStyle(
          fontSize: 36.0,
          fontFamily: 'Horizon',
          fontWeight: FontWeight.bold,
          color: Colors.blue,
        ),
        child: AnimatedTextKit(
          animatedTexts: texts.map((e) => RotateAnimatedText(e)).toList(),
          onTap: () {
            print("文字点击事件");
          },
          repeatForever: true,
        ),
      ),
    );
  }

  Widget typerText(List<String> texts) {
    return SizedBox(
      width: 320.0,
      child: DefaultTextStyle(
        style: const TextStyle(
          fontSize: 30.0,
          color: Colors.blue,
        ),
        child: AnimatedTextKit(
          animatedTexts: texts
              .map((e) => TyperAnimatedText(
                    e,
                    textAlign: TextAlign.start,
                    speed: Duration(milliseconds: 300),
                  ))
              .toList(),
          onTap: () {
            print("文字点击事件");
          },
          repeatForever: true,
        ),
      ),
    );
  }
}

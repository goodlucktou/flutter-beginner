import 'package:flutter/material.dart';

import 'windmill_indicator.dart';

class WindmillIndicatorDemo extends StatelessWidget {
  const WindmillIndicatorDemo({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('大风车'),
      ),
      body: Center(
        child: WindmillIndicator(
          size: 100.0,
          speed: 0.5,
          direction: RotationDirection.clockwise,
        ),
      ),
    );
  }
}

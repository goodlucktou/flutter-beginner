import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'round_image.dart';

class FlightShuttleBuilderHero extends StatelessWidget {
  const FlightShuttleBuilderHero({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //timeDilation = 5.0;
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero 飞行过程控制'),
        brightness: Brightness.dark,
      ),
      body: Container(
        child: Hero(
          flightShuttleBuilder: (_, animation, direction, __, ___) {
            if (direction == HeroFlightDirection.push) {
              return ClipOval(
                child: Opacity(
                  child: Container(color: Colors.pink),
                  opacity: 0.4,
                ),
              );
            } else {
              return ClipOval(
                child: Opacity(
                  child: Container(color: Colors.blue),
                  opacity: 0.4,
                ),
              );
            }
          },
          transitionOnUserGestures: true,
          child: RoundImage(
            assetImageName: 'images/beauty.jpeg',
            imageSize: 100.0,
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (context) => Scaffold(
                    appBar: AppBar(
                      title: Text('Hero 飞行过程控制'),
                      brightness: Brightness.dark,
                    ),
                    body: Center(
                      child: Hero(
                        tag: 'beauty',
                        child: RoundImage(
                          assetImageName: 'images/beauty2.jpeg',
                          imageSize: 200.0,
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        transitionOnUserGestures: true,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
          tag: 'beauty',
        ),
      ),
    );
  }
}

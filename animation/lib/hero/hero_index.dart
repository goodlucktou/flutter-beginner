import 'package:animation/hero/hero_detail.dart';
import 'package:flutter/material.dart';

import 'round_image.dart';

class HeroIndexPage extends StatelessWidget {
  const HeroIndexPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hero 基础动画'),
        brightness: Brightness.dark,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
              tag: 'beauty1',
              child: RoundImage(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => HeroDetail(
                        tag: 'beauty1',
                        assetImageName: 'images/beauty.jpeg',
                      ),
                    ),
                  );
                },
                assetImageName: 'images/beauty.jpeg',
                imageSize: 80.0,
              ),
            ),
            Hero(
              tag: 'beauty2',
              child: RoundImage(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => HeroDetail(
                        tag: 'beauty2',
                        assetImageName: 'images/beauty2.jpeg',
                      ),
                    ),
                  );
                },
                assetImageName: 'images/beauty2.jpeg',
                imageSize: 80.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

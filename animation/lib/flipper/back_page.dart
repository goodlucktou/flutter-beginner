import 'package:flutter/material.dart';

class BackPage extends StatelessWidget {
  const BackPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: Center(
        child: Container(
          width: 128,
          height: 128,
          margin: EdgeInsets.only(top: 10, bottom: 10),
          child: Image.asset(
            'images/qrcode.jpg',
          ),
        ),
      ),
    );
  }
}

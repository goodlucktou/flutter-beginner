import 'package:flutter/material.dart';

import 'bloc_builder/bloc_builder_demo.dart';
import 'bloc_consumer/bloc_consumer_demo.dart';
import 'bloc_listener/bloc_listener_demo.dart';
import 'bloc_provider/bloc_counter.dart';
import 'bloc_repository_provider/personal_homepage_repository.dart';
import 'juejin/personal_homepage.dart';
import 'simple_bloc_provider/simple_bloc_counter.dart';
import 'utils/global_dialogs.dart';

void main() {
  runApp(MyApp());
}

// SimpleBlocCounterPage：自己写的 BlocProvider 计数器
// BlocCounterWrapper：官方的 BlocProvider 计数器示例
// BlocBuilderDemoPage：BlocBuilder 示例
// BlocListenerWrapper：BlocListener 示例
// BlocConsumerWrapper： BlocConsumer 示例
// PersonalHomePageRepository：BlocRepository 示例
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PersonalHomePageRepository(),
      builder: GlobalDialogs.initDialogs(),
    );
  }
}

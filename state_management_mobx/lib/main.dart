import 'package:flutter/material.dart';
import 'charts/line_chart.dart';
import 'counter/counter.dart';
import 'mobx_share/dynamic_detail_agly.dart';
import 'mobx_share/dynamic_detail_provider.dart';
import 'mobx_share/dynamic_detail_getit.dart';
import 'utils/cookie_manager.dart';

void main() {
  runApp(MyApp());
  CookieManager.instance.initCookie();
}

/// MobX示例
/// counter：MobX 计数器
/// line_chart：通过网络数据渲染曲线
/// dynamic_detail_agly：状态共享例子：逐级传递
/// dynamic_detail_provider：状态共享例子：使用 Provider
/// dynamic_detail_getit：状态共享例子：使用 GetIt 容器
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MobX Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DynamicDetailGetIt(),
    );
  }
}

import 'package:mobx/mobx.dart';

import 'chart_service.dart';

part 'chart_store.g.dart';

class ChartStore = ChartStoreBase with _$ChartStore;

abstract class ChartStoreBase with Store {
  @observable
  List<double> lineYData = [0, 0, 0, 0, 0, 0, 0];

  @action
  Future<void> featchLineData() async {
    var response = await ChartService.getLines();
    if (response?.statusCode == 200) {
      lineYData =
          List<double>.from(response.data.map((e) => e.toDouble()).toList());
    }
  }
}

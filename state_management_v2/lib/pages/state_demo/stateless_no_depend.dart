import 'package:flutter/material.dart';

class StatelessNoDepend extends StatelessWidget {
  StatelessNoDepend({Key? key}) : super(key: key) {
    print('constructor: 不依赖状态的组件');
  }

  @override
  Widget build(BuildContext context) {
    print('build：不依赖于状态的组件');
    return Center(
      child: Text('这是不依赖于状态的组件'),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:home_framework/pages/state_demo/stateless_leisi.dart';
import 'package:home_framework/pages/state_demo/stateless_no_depend.dart';
import 'package:home_framework/pages/state_demo/stateless_xiaofu.dart';
import 'package:home_framework/states/face_emotion.dart';

class SetStateDemo extends StatefulWidget {
  SetStateDemo({Key? key}) : super(key: key);

  _SetStateDemoState createState() => _SetStateDemoState();
}

class _SetStateDemoState extends State<SetStateDemo> {
  FaceEmotion faceEmotion = FaceEmotion();

  void updateEmotion(FaceEmotion newEmotion) {
    if (faceEmotion != newEmotion) {
      setState(() {
        faceEmotion = newEmotion;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('setState 方式'),
      ),
      body: Column(
        children: [
          StatelessXiaofu(face: faceEmotion, updateEmotion: updateEmotion),
          StatelessLeisi(face: faceEmotion),
          StatelessNoDepend(),
        ],
      ),
    );
  }
}

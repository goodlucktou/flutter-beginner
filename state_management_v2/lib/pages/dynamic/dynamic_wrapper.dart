import 'package:flutter/material.dart';
import 'dynamic.dart';

class DynamicWrapper extends StatelessWidget {
  const DynamicWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DynamicPage();
  }
}

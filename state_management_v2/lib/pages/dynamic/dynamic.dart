import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:home_framework/routers/fluro_router.dart';
import '../../view_models/dynamic/dynamic_model.dart';
import 'package:provider/provider.dart';
import 'dynamic_item.dart';

class DynamicPage extends StatelessWidget {
  DynamicPage({Key? key}) : super(key: key);

  final EasyRefreshController _refreshController = EasyRefreshController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('动态', style: Theme.of(context).textTheme.headline4),
        actions: [
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                RouterManager.router
                    .navigateTo(context, RouterManager.dynamicAddPath);
              }),
        ],
        brightness: Brightness.dark,
      ),
      body: EasyRefresh(
        controller: _refreshController,
        firstRefresh: true,
        onRefresh: () async {
          context.read<DynamicModel>().refresh();
        },
        onLoad: () async {
          context.read<DynamicModel>().load();
        },
        child: ListView.builder(
          itemCount: context.watch<DynamicModel>().dynamics.length,
          itemBuilder: (context, index) {
            return DynamicItem(context.watch<DynamicModel>().dynamics[index],
                (String id) {
              context.read<DynamicModel>().removeWithId(id);
            });
          },
        ),
      ),
    );
  }
}

本源码是在掘金专栏：[Flutter 入门与实战](https://juejin.cn/column/6960631670378594311)的示例代码，对应篇章为：[Flutter入门与实战（二十一）](https://juejin.cn/post/6978485077554511886)。欢迎大家前往掘金点赞支持，或在评论区交流。也欢迎关注本人公众号：岛上码农。

![qrcode_island_coder.jpg](https://gitee.com/island-coder/flutter-beginner/raw/master/qrcode_island_coder.jpg)

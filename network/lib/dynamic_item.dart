import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:home_framework/models/dynamic_entity.dart';
import 'package:home_framework/routers/fluro_router.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';

class DynamicItem extends StatelessWidget {
  final DynamicEntity dynamicEntity;
  final ValueChanged<String> handleDelete;
  static const double ITEM_HEIGHT = 100;
  static const double TITLE_HEIGHT = 80;
  static const double MARGIN_SIZE = 10;
  const DynamicItem(this.dynamicEntity, this.handleDelete, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FocusedMenuHolder(
      child: Container(
        margin: EdgeInsets.all(MARGIN_SIZE),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _imageWrapper(this.dynamicEntity.imageUrl),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _titleWrapper(context, dynamicEntity.title),
                  _viewCountWrapper(dynamicEntity.viewCount.toString()),
                ],
              ),
            )
          ],
        ),
      ),
      onPressed: () {
        _handlePressed(context);
      },
      menuItems: <FocusedMenuItem>[
        FocusedMenuItem(
          title: Text("查看详情"),
          trailingIcon: Icon(Icons.remove_red_eye_outlined),
          onPressed: () {
            _handlePressed(context);
          },
        ),
        FocusedMenuItem(
            title: Text("编辑"),
            trailingIcon: Icon(Icons.edit),
            onPressed: () {
              RouterManager.router.navigateTo(context,
                  '${RouterManager.dynamicEditPath.replaceAll('/:id', '')}/${dynamicEntity.id}');
            }),
        FocusedMenuItem(
          title: Text("取消"),
          trailingIcon: Icon(Icons.cancel),
          onPressed: () {},
        ),
        FocusedMenuItem(
            title: Text(
              "删除",
              style: TextStyle(color: Colors.redAccent),
            ),
            trailingIcon: Icon(
              Icons.delete,
              color: Colors.redAccent,
            ),
            onPressed: () {
              handleDelete(dynamicEntity.id);
            }),
      ],
    );
  }

  void _handlePressed(context) async {
    var arguments = await RouterManager.router.navigateTo(
        context, '${RouterManager.dynamicPath}/${dynamicEntity.id}');
    if (arguments != null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("从动态${(arguments as Map<String, dynamic>)['id']}返回"),
      ));
    }
  }

  Widget _titleWrapper(BuildContext context, String text) {
    return Container(
      height: TITLE_HEIGHT,
      margin: EdgeInsets.fromLTRB(MARGIN_SIZE, 0, 0, 0),
      child: Text(
        dynamicEntity.title,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _viewCountWrapper(String text) {
    return Container(
      margin: EdgeInsets.fromLTRB(MARGIN_SIZE, 0, 0, 0),
      height: ITEM_HEIGHT - TITLE_HEIGHT,
      child: Row(children: [
        Icon(
          Icons.remove_red_eye_outlined,
          size: 14.0,
          color: Colors.grey,
        ),
        SizedBox(width: 5),
        Text(
          dynamicEntity.viewCount.toString(),
          style: TextStyle(color: Colors.grey, fontSize: 14.0),
        ),
      ]),
    );
  }

  Widget _imageWrapper(String imageUrl) {
    return SizedBox(
      width: 150,
      height: ITEM_HEIGHT,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            LinearProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) =>
            Image.asset('images/image-failed.png'),
      ),
    );
  }
}

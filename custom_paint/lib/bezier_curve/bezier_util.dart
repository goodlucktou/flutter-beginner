import 'package:flutter/material.dart';

class BezierUtil {
  static Offset get2OrderBezierPoint(
      Offset p1, Offset p2, Offset p3, double t) {
    var x = (1 - t) * (1 - t) * p1.dx + 2 * t * (1 - t) * p2.dx + t * t * p3.dx;
    var y = (1 - t) * (1 - t) * p1.dy + 2 * t * (1 - t) * p2.dy + t * t * p3.dy;

    return Offset(x, y);
  }

  static Offset get3OrderBezierPoint(
      Offset p1, Offset p2, Offset p3, Offset p4, double t) {
    var x = (1 - t) * (1 - t) * (1 - t) * p1.dx +
        3 * t * (1 - t) * (1 - t) * p2.dx +
        3 * t * t * (1 - t) * p3.dx +
        t * t * t * p4.dx;
    var y = (1 - t) * (1 - t) * (1 - t) * p1.dy +
        3 * t * (1 - t) * (1 - t) * p2.dy +
        3 * t * t * (1 - t) * p3.dy +
        t * t * t * p4.dy;

    return Offset(x, y);
  }
}

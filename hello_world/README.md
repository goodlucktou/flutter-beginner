# 第一个 Flutter 程序

实现点击按钮切换图片。

代码逻辑如下：
\_MyHomePageState 类是 MyHomePage 的 一个有状态组件，Flutter 的组件和 React 的类似，分为无状态和有状态，无状态组件无法进行数据更新，有状态组件有自己的数据状态，根据数据状态更新界面。

下划线\_代表这个类、方法或成员变量是私有的，在类的外部无法访问。在\_MyHomePageState 中定义了一个 Map <String, String>类型的数组（List）\_imageNames 以及一个状态变量数组控制下标\_index。当切换按钮点击时，会在\_onSwitch 方法中通过 setState 更改状态变量\_index 的值，从而引起界面的自动刷新。

页面组件的元素和层级如下：

-   appBar：导航栏
-   body：主界面
    -   Center：居中组件
        -   Container：页面元素容器，类似 html 的 div，通过这个 Container 指定界面中的尺寸和边距
            -   Column：纵向布局，即元素按纵向一次排布。
                -   图片容器：用于限定图片的显示大小，边距等
                    -   图片：使用本地资源展示图片
                -   文本：显示图片底下的文字
-   floatingActionButton：悬浮按钮

可以看到整个页面的层次和 HTML 很像，实际上 Dart 最开始的设计就是想替换 Javascript 的，而 Flutter 本身很多理念仿照了 React。从代码也可以看到，界面的嵌套层级很多，这被很多人吐槽，实际只要我们将组件抽离，就可以有效减少嵌套层级（界面的写法也有点类似 JSX，只是 Flutter 内置了很多布局组件，简化了开发）。

#### 最终效果

![helloworld.jpg](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/8622cf63e9d44d15adbe532e208d3204~tplv-k3u1fbpfcp-watermark.image)
点击切换按钮图片和文字会随之变化

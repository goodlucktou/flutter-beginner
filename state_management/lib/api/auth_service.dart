import '../utils/http_util.dart';

class AuthService {
  static String host = 'http://localhost:3900/api/auth/';

  static Future login(String username, String password) async {
    var result = await HttpUtil.post(
      host + 'login',
      data: {'email': username, 'password': password},
    );

    return result;
  }

  static Future logout() async {
    var result = await HttpUtil.post(host + 'logout');

    return result;
  }

  static Future checkSession() async {
    var result = await HttpUtil.get(host + 'check');

    return result;
  }
}
